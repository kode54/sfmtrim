#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "gme.h"

const unsigned int minimum_log_repeat_count = 128;

struct expanding_buffer
{
    uint8_t * buffer;
    size_t size;
};

gme_err_t write_to_memory( void* _buffer, void const* in, long count )
{
    struct expanding_buffer * buffer = ( struct expanding_buffer * ) _buffer;
    uint8_t * new_buffer = ( uint8_t * ) realloc( buffer->buffer, buffer->size + count );
    if ( !new_buffer ) return "Out of memory";
    buffer->buffer = new_buffer;
    memcpy( buffer->buffer + buffer->size, in, count );
    buffer->size += count;
    return gme_ok;
}

static uint32_t get_le32( void const* _p )
{
    uint8_t const* p = ( uint8_t const* ) _p;
    return p[0] | (p[1] << 8) | (p[2] << 16) | (p[3] << 24);
}

int main(int argc, char ** argv)
{
    const char * system;
    gme_t * emu;
    gme_err_t err;
    FILE * f;
    
    char * end;
    unsigned long minimum_skip = 0;
    unsigned long trim_count = 0;
    
    short sample_buffer[512 * 2];
    int last_sample_left, last_sample_right;
    int delta_left, delta_right;
    unsigned long skipped_silence;
    
    struct expanding_buffer buffer = { 0, 0 };
    
    struct expanding_buffer trim_buffer = { 0, 0 };
    
    int i;
    
    unsigned long j;
    
    uint8_t *log_begin, *log_end, *loop_begin, *loop_end;
    
    if ( argc < 3 || argc > 5 )
    {
        fputs( "Syntax:\tsfmtrim <input.sfm> <output.sfm> [count] [count]\n"
               "\t- The first count is how many samples to forcibly skip\n"
               "\t  before performing any silence detection.\n"
               "\t- The second count is how many samples worth of log\n"
               "\t  should go into the output. Any further log will be\n"
               "\t  trimmed off.\n\n", stderr );
        return 1;
    }
    
    if ( argc >= 4 )
        minimum_skip = strtoul( argv[3], &end, 10 ) * 2;
    
    if ( argc >= 5 )
        trim_count = strtoul( argv[4], &end, 10 ) * 2;
    
    err = gme_open_file( argv[1], &emu, 32000 );
    if ( err != gme_ok )
    {
        fprintf( stderr, "Error: %s\n", err );
        return 1;
    }
    
    if ( strcmp( ( system = gme_type_system( gme_type( emu ) ) ), "Super Nintendo with log" ) != 0 )
    {
        fprintf( stderr, "Not an SFM file, actually %s.\n", system );
        gme_delete( emu );
        return 1;
    }
    
    gme_ignore_silence( emu, 1 );
    
    err = gme_start_track( emu, 0 );
    if ( err )
    {
        gme_delete( emu );
        fprintf( stderr, "Error starting track: %s\n", err );
        return 1;
    }
    
    if ( minimum_skip )
    {
        fprintf( stderr, "Skipping %lu samples first.\n", minimum_skip / 2 );
        gme_skip( emu, (int) minimum_skip );
    }
    
    skipped_silence = 0;
    
    last_sample_left = 0; last_sample_right = 0;
    
    while ( !gme_track_ended( emu ) )
    {
        err = gme_play( emu, 1024, sample_buffer );
        if ( err != gme_ok ) break;
        for ( i = 0; i < 1024; i += 2 )
        {
            delta_left = sample_buffer[i] - last_sample_left;
            last_sample_left = sample_buffer[i];
            delta_right = sample_buffer[i + 1] - last_sample_right;
            last_sample_right = sample_buffer[i + 1];
            if ( ( ( unsigned )( delta_left + 8 ) >= 8 * 2 ) ||
                ( ( unsigned )( delta_right + 8 ) >= 8 * 2 ) )
                break;
        }
        skipped_silence += i;
        if ( i < 1024 ) break;
    }
    
    if ( err != gme_ok )
    {
        gme_delete( emu );
        fprintf( stderr, "Emulation error: %s\n", err );
        return 1;
    }
    
    /* Preserve 128 silence samples, so hopefully whatever log event starts
     * the playback will exist in the output file.
     */
    if ( skipped_silence >= 256 )
        skipped_silence -= 256;
    
    gme_start_track( emu, 0 );
    
    err = gme_skip( emu, (int)( minimum_skip + skipped_silence ) );
    if ( err )
    {
        gme_delete( emu );
        fprintf( stderr, "Error skipping samples: %s\n", err );
        return 1;
    }
    
    fprintf( stderr, "Skipped %lu silence samples", skipped_silence / 2 );
    if ( minimum_skip )
        fprintf( stderr, ", in addition to %lu requested samples", minimum_skip / 2 );
    fputs( ".\n", stderr );
    
    err = gme_save( emu, write_to_memory, &buffer );
    if ( err )
    {
        if ( buffer.buffer ) free( buffer.buffer );
        gme_delete( emu );
        fprintf( stderr, "Error generating SFM to memory: %s\n", err );
        return 1;
    }
    
    if ( trim_count )
    {
        fprintf( stderr, "Trimming log to a maximum of %lu samples.\n", trim_count / 2 );
        
        err = gme_skip( emu, (int)trim_count );
        if ( err )
        {
            free( buffer.buffer );
            gme_delete( emu );
            fprintf( stderr, "Error skipping samples: %s\n", err );
            return 1;
        }
        
        err = gme_save( emu, write_to_memory, &trim_buffer );
        if ( err )
        {
            if ( trim_buffer.buffer ) free( trim_buffer.buffer );
            free( buffer.buffer );
            gme_delete( emu );
            fprintf( stderr, "Error writing trimmed file to memory: %s\n", err );
            return 1;
        }
        
        j = 4 + 4 + get_le32( trim_buffer.buffer + 4 ) + 65536 + 128;
        if ( j < trim_buffer.size )
        {
            j = trim_buffer.size - j;
            fprintf( stderr, "Trimming %lu entries off the end of the log.\n", j );
            buffer.size -= j;
        }
        free( trim_buffer.buffer );
    }
    
    gme_delete( emu );

#if 0
    j = 4 + 4 + get_le32( buffer.buffer + 4 ) + 65536 + 128; // header + meta_size + meta + SMP RAM + DSP registers
    if ( j >= buffer.size )
    {
        fputs( "No log data in resulting file, saving as-is.\n", stderr );
        goto save_output;
    }
    
    log_begin = buffer.buffer + j;
    log_end = buffer.buffer + buffer.size;
    loop_end = log_end - minimum_log_repeat_count;
    loop_begin = log_begin;
    
    while ( loop_begin <= loop_end - minimum_log_repeat_count )
    {
        if ( memcmp( loop_begin, loop_end, minimum_log_repeat_count ) == 0 ) break;
        ++loop_begin;
    }
    
    if ( loop_begin > loop_end - minimum_log_repeat_count )
    {
        fputs( "Log doesn't appear to loop, saving as-is.\n", stderr );
        goto save_output;
    }
    
    fprintf( stderr, "Loop start found at offset %lu, searching for earliest repeat.\n", loop_begin - log_begin );
    
    loop_end = loop_begin + minimum_log_repeat_count;

    while ( loop_end < log_end - minimum_log_repeat_count )
    {
        if ( memcmp( loop_begin, loop_end, minimum_log_repeat_count ) == 0 ) break;
        ++loop_end;
    }
    
    if ( loop_end < log_end - minimum_log_repeat_count )
    {
        fprintf( stderr, "Log shortened to %lu entries.\n", loop_end - log_begin );
        buffer.size = loop_end - buffer.buffer;
    }
    else
    {
        fputs( "Unable to shorten log.\n", stderr );
    }
#endif
    
save_output:
    f = fopen( argv[2], "wb" );
    if ( !f )
    {
        fputs( "Error opening output file.\n", stderr );
        free( buffer.buffer );
        return 1;
    }
    
    if ( fwrite( buffer.buffer, 1, buffer.size, f ) < buffer.size )
    {
        fclose( f );
        free( buffer.buffer );
        fputs( "Error writing output file.\n", stderr );
        return 1;
    }
    
    fclose( f );
    free( buffer.buffer );
    
    fputs( "Done.\n", stderr );
    
    return 0;
}
