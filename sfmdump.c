#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "gme.h"

int main(int argc, char ** argv)
{
    const char * system;
    gme_t * emu;
    gme_err_t err;
    FILE * f;
    char * end;
    
    short sample_buffer[512 * 2];
    
    unsigned long samples_rendered, samples_requested;
    
    if ( argc != 4 )
    {
        fputs( "Syntax:\tsfmdump <input.sfm> <output.raw> <samples to render>\n", stderr );
        return 1;
    }
    
    samples_requested = strtoul( argv[3], &end, 10 ) * 2;
    
    err = gme_open_file( argv[1], &emu, 32000 );
    if ( err != gme_ok )
    {
        fprintf( stderr, "Error: %s\n", err );
        return 1;
    }
    
    if ( strcmp( ( system = gme_type_system( gme_type( emu ) ) ), "Super Nintendo with log" ) != 0 )
    {
        fprintf( stderr, "Not an SFM file, actually %s.\n", system );
        gme_delete( emu );
        return 1;
    }
    
    gme_ignore_silence( emu, 1 );
    
    err = gme_start_track( emu, 0 );
    if ( err )
    {
        gme_delete( emu );
        fprintf( stderr, "Error starting track: %s\n", err );
        return 1;
    }
    
    f = fopen( argv[2], "wb" );
    if ( !f )
    {
        gme_delete( emu );
        fputs( "Error opening output file.\n", stderr );
        return 1;
    }
    
    samples_rendered = 0;
    
    while ( !gme_track_ended( emu ) && samples_rendered < samples_requested )
    {
        unsigned int samples_to_render = 1024;
        if ( samples_to_render > (samples_requested - samples_rendered ) )
            samples_to_render = (int)( samples_requested - samples_rendered );
        samples_rendered += samples_to_render;
        err = gme_play( emu, samples_to_render, sample_buffer );
        if ( err != gme_ok ) break;
        fwrite( sample_buffer, 2, samples_to_render, f );
    }
    
    gme_delete( emu );
    fclose( f );
    
    if ( err != gme_ok )
    {
        fprintf( stderr, "Emulation error: %s\n", err );
        return 1;
    }
    
    fputs( "Done.\n", stderr );
    
    return 0;
}
