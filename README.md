These tools are based on the preliminary version of the SFM format, a
proposed replacement for the SPC dump format intended to support
capturing game soundtracks previously unrippable.

It currently contains a full state snapshot of the SNES S-SMP, according
to all of the state variables of the Higan "accuracy" S-SMP core, which
can mostly downgrade to less accurate S-SMP emulators, except that the
timer state may contain a little more information than less accurate or
faster emulators may support. Using a less accurate S-SMP core may not
work exactly right, but time will tell. I have since expanded this data
to include the full S-SMP registers, as they are needed to produce dumps
from anywhere post-reset. I also added the last state of the read side
$0F4-$0F7 ports, in the event that the log (read further) is trimmed off.

It also contains a full state dump of the SNES S-DSP core, compatible
with either blargg's cycle accurate snes_spc core, Higan "alt" S-DSP
core, which is also based on blargg's cycle accurate core, or Higan
"accuracy" S-DSP. There is no guarantee that it will downgrade properly
to less accurate cores, but that should be immaterial, as less accurate
cores such as blargg's "fast" snes_spc core already have problems
playing back plain SPC dumps from games such as Earthworm Jim. I have
since added the current S-DSP cycle count offset from the S-SMP cycle
count.

It then contains the APU RAM and S-DSP register blocks, which can be
restored without pulling the S-SMP or S-DSP state variables from within,
as the above meta block contains the full internal state already.

Then, finally, it contains a log of port data which will be fed one byee
at a time to each read request to $0F4-$0F7, until the data is exhausted.
I extended the initial specification to include a loop start offset into
this log data where the log will continue when it has been exhausted by
the player. Where this loop start address is stored in the meta block
has not been finalized, and none of these tools support writing to it yet
anyway.

I also extended the meta block to include several informational fields
beyond just the track title. I have not yet added fields for volume
scaling such as ReplayGain, as I have not wanted to extend Game_Music_Emu
to include reading that just yet. Doing that will mean that I may have to
add an APE tag reader for SPC files as well, since those are the only
unofficially supported tags which are capable of storing ReplayGain tags.

Feedback is appreciated, but do not start ripping willy nilly, as the
format is not finalized, and I don't want to have to either support older
rips or provide a converter to upgrade older rips to a newer version of
the format. Experiment at your own risk.

